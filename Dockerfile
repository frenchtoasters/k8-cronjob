FROM python:3.6

COPY test.py /

CMD ["/usr/local/bin/python", "/test.py", "Now"]
